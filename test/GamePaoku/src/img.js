export default class img extends Laya.Script {

    constructor() {
        super();
    }

    onEnable() {
        /* 坐标数组 */
        var arr = this.owner.shan._graphics._one.points;
        /* 遮罩的X坐标 */
        var shanX = this.owner.shan._graphics._one.x;
        /* 遮罩的Y坐标 */
        var shanY = this.owner.shan._graphics._one.y;
        for (let index = 0; index < arr.length; index++) {
            /* 每两个数是一个节点 */
            if (index % 2 == 0) {
                if (arr[index] > 0) {
                    // var index = 0;
                    /* 获取两个点之间的距离(勾股定理) */
                    var gouA = arr[index + 2] - arr[index];
                    var gouB = arr[index + 3] - arr[index + 1];
                    /* 渲染草地 */
                    var a = this.getAngle(arr[index], arr[index + 1], arr[index + 2], arr[index + 3]) - 90;
                    this.addCao(arr[index], arr[index + 1], gouA, gouB, shanX, shanY, a)
                    console.log(a)
                }
            }
        }
    }

    /* 渲染草地图片 */

    addCao(beginX, beginY, A, B, pianX, pianY, a) {
        var C = Math.sqrt(A * A + B * B);
        /* 图片数量 */
        var amount = Math.ceil(C / 71);
        /* 图片长度 */
        var imgLength = C / (Math.ceil(C / 71));
        /* 增值 */
        var addX = A / amount;
        var addY = B / amount;
        for (let index = 0; index < amount; index++) {
            var img = new Laya.Image('comp/graphics1535.png');
            img.width = imgLength;
            img.height = 12;
            this.owner.caoBox.addChild(img);
            // img
            img.rotation = a;
            var pianyix = 0;
            if (a < 0) {
                pianyix = -5;
            } else {
                pianyix = 10;
            }
            img.pos(beginX + pianX + index * addX + pianyix, beginY + pianY + index * addY - 12);
        }
        // var img = new Laya.Image('comp/graphics1535.png');
        // img.width = 71;
        // img.height = 12;
        // this.owner.caoBox.addChild(img);
        // img.pos(beginX + pianX, beginY + pianY - 12);

    }
    getAngle(px, py, mx, my) {//获得人物中心和鼠标坐标连线，与y轴正半轴之间的夹角
        var x = Math.abs(px - mx);
        var y = Math.abs(py - my);
        var z = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
        var cos = y / z;
        var radina = Math.acos(cos);//用反三角函数求弧度
        var angle = Math.floor(180 / (Math.PI / radina));//将弧度转换成角度

        if (mx > px && my > py) {//鼠标在第四象限
            angle = 180 - angle;
        }
        if (mx == px && my > py) {//鼠标在y轴负方向上
            angle = 180;
        }
        if (mx > px && my == py) {//鼠标在x轴正方向上
            angle = 90;
        }
        if (mx < px && my > py) {//鼠标在第三象限
            angle = 180 + angle;
        }
        if (mx < px && my == py) {//鼠标在x轴负方向
            angle = 270;
        }
        if (mx < px && my < py) {//鼠标在第二象限
            angle = 360 - angle;
        }
        return angle;
    }
    onDisable() {
    }
}
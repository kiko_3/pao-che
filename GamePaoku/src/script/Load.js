export default class Load extends Laya.Scene {

    constructor() {
        super();
        //加载Laya场景数据，创建场景
        this.loadScene('Gameload.scene');
    }

    onEnable() {
        this.starBtn.on(Laya.Event.CLICK, this, this.on_starBtn)
    }
    on_starBtn() {
        this.close()
    }
    onDisable() {
    }
}